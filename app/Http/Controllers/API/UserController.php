<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Event;
use App\Models\FriendRequest;
use App\Models\Post;
use App\Models\UserActivity;
use App\Models\LogActivity;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

use Log;

class UserController extends Controller
{

    public function concurrent_check()
    {
        $time_start = microtime(true);

        $client = new Client();
        $promises = [
            'Allusers' => $client->getAsync('http://localhost/user_activity_project/public/api/users'),

            'post1' => $client->getAsync('http://localhost/user_activity_project/public/api/posts/1'),

            'user1' => $client->getAsync('http://localhost/user_activity_project/public/api/users/1'),
            'user1_Logs' => $client->getAsync('http://localhost/user_activity_project/public/api/users/logs/1'),
            'user2_Logs' => $client->getAsync('http://localhost/user_activity_project/public/api/users/logs/1'),
            'user2'   => $client->getAsync('http://localhost/user_activity_project/public/api/users/logs/2'),
            'user3'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/3'),
            'user4'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/4'),

            'Allposts' => $client->getAsync('http://localhost/user_activity_project/public/api/posts'),

            'user5'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/5'),
            'user6'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/6'),
            'user7'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/7'),
            'user8'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/8'),
            'user9'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/9'),
            'user10'  => $client->getAsync('http://localhost/user_activity_project/public/api/users/10'),
        ];

        $responses = Promise\Utils::settle($promises)->wait();

        foreach ($responses as $key => $response)
        {
            echo $key . "<br>";

            //response state is either 'fulfilled' or 'rejected'
            if($response['state'] === 'rejected')
            {
                //handle rejected
                continue;
            }
            //$result is a Guzzle Response object
            $result = $response['value'];
            //do stuff with $result
            //eg. $result->getBody()
        }

        echo '<h3>Concurrent execution time in seconds: </h3>';
        echo '<h1>'.(microtime(true) - $time_start).'</h1>';
    }

    public function sequential_check()
    {

        $time_start = microtime(true);

        // $method1 = [];

        $responses = [
            'Allposts' => Http::get('http://localhost/user_activity_project/public/api/posts'),
            'Allusers' => Http::get('http://localhost/user_activity_project/public/api/users'),
            'post1' => Http::get('http://localhost/user_activity_project/public/api/posts/1'),
            'user1' => Http::get('http://localhost/user_activity_project/public/api/users/1'),
            'user1_Logs' => Http::get('http://localhost/user_activity_project/public/api/users/logs/1'),
            'user2'   => Http::get('http://localhost/user_activity_project/public/api/users/logs/2'),
            'user2_Logs' => Http::get('http://localhost/user_activity_project/public/api/users/logs/1'),
            'user3'  => Http::get('http://localhost/user_activity_project/public/api/users/3'),
            'user4'  => Http::get('http://localhost/user_activity_project/public/api/users/4'),
            'user5'  => Http::get('http://localhost/user_activity_project/public/api/users/5'),
            'user6'  => Http::get('http://localhost/user_activity_project/public/api/users/6'),
            'user7'  => Http::get('http://localhost/user_activity_project/public/api/users/7'),
            'user8'  => Http::get('http://localhost/user_activity_project/public/api/users/8'),
            'user9'  => Http::get('http://localhost/user_activity_project/public/api/users/9'),
            'user10'  => Http::get('http://localhost/user_activity_project/public/api/users/10'),
        ];

        foreach ($responses as $key => $response)
        {
            echo $key . "<br>";
        }

        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/1');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/2');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/3');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/4');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/5');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/6');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/7');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/8');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/9');
        // $method1 = Http::get('http://localhost/user_activity_project/public/api/users/10');

        // print_r($method1);

        echo '<h3>Normal execution time in seconds: </h3>';
        echo '<h1>'.(microtime(true) - $time_start).'</h1>';
        

        // return response()->json([
        //     'data' => $method1,
        //     'time' => $execution_time,
        //     'status' => 200,
        // ]);
    
        // $responses = Http::pool(fn (Pool $pool) => [
        //     $pool->get('http://localhost/user_activity_project/public/api/users/1'),
        //     $pool->get('http://localhost/user_activity_project/public/api/users/2'),
        //     $pool->get('http://localhost/user_activity_project/public/api/users/3'),
        // ]);
        

        // return $responses[0]->ok() &&
        // $responses[1]->ok() &&
        // $responses[2]->ok();
    
    }

    // Latest Modified Code :: 21/05/2021
    public function logActivity(Request $request)
    {

        $perPage = 2;
        $page = $request->page? $perPage * ($request->page-1) : 0;

        $log_activities =  LogActivity::where('user_id', auth()->user()->id)
                                        ->take($perPage)
                                        ->skip($page)
                                        ->orderBy('created_at', 'desc')
                                        ->get();
        
        $log_activities->transform(function($item){

            if($item->log_type == 'Post')
            {
                return $item->post;
            } 
            else if($item->log_type == 'Event')
            {
                return $item->event;
            }
            else if($item->log_type == 'Activity')
            {
                return $item->userActivity;
            }
            else if($item->log_type == 'Request')
            {
                return $item->friendRequest;
            }

        });

        return response()->json([
            'log_activities' => $log_activities,
            'page' => $page,
            'status' => 200,
        ]);
    }


    // Old Modified Code :: 20/05/2021
    public function logActivity2Old(Request $request)
    {

        $user_logs = []; 

        $perPage = 2;
        $page = $request->page? $perPage * ($request->page-1) : 0;

        $log_activities =  LogActivity::where('user_id', auth()->user()->id)->take($perPage)->skip($page)->orderBy('created_at', 'desc')->get();

        foreach($log_activities as $log_activity)
        {
            if($log_activity->log_type == 'Post')
            {
                array_push($user_logs, $log_activity->post);
            } 
            else if($log_activity->log_type == 'Event')
            {
                array_push($user_logs, $log_activity->event);
            }
            else if($log_activity->log_type == 'Activity')
            {
                array_push($user_logs, $log_activity->userActivity);
            }
            else if($log_activity->log_type == 'Request')
            {
                array_push($user_logs, $log_activity->friendRequest);
            }
        }
        
        return response()->json([
            'log_activities' => $user_logs,
            'page' => $page,
            'status' => 200,
        ]);
    }
    
    public function userLogs($id)
    {
        if(\Auth::user())
        {

            $user = User::select('id','name', 'email')->where('id', $id)->get();

            $posts          = Post::where('user_id', $id)->get();
            $user_activity  = UserActivity::where('user_id', $id)->get();
            $event          = Event::where('user_id', $id)->get();
            $friend_request = FriendRequest::where('user_id', $id)->get();

            $user_log = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the merge() method
            
            $user_log = $user_log->merge($posts);
            $user_log = $user_log->merge($user_activity);
            $user_log = $user_log->merge($event);
            $user_log = $user_log->merge($friend_request);

            $user_log = $user_log->sortBy([
                ['created_at', 'desc']
            ]);
            
            $user_log = $user_log->paginate(8);

            return response()->json([
                'user' => $user,
                'user_logs' => $user_log,
                'status' => 200,
            ]);
            
        }
        else
        {
            return response(['message'=> 'You are not logged in', 'status'=>201]);
        }
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user())
        {
            $users = User::all();
            return response(['users'=> $users, 'status'=>200]);
        }
        else
        {
            return response(['message'=> 'You are not logged in', 'status'=>201]);
        }
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        // $user->user_posts = $user->posts()->orderBy('created_at', 'desc')->get();

        return response()->json([
            'user' => $user,
            'status' => 200,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
