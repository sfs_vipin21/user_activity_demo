<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    use HasFactory;

    protected $table = 'user_friend_requests';

    protected $fillable = [
        'user_id',
        'to_user_id', 
        'request_status',
    ];
    
    public function user()
    {
       return $this->belongsTo(User::class);
    }

}
