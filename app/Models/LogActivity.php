<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use HasFactory;

    protected $table = 'log_activities';

    protected $fillable = [
        'user_id', 
        'log_id', 
        'log_type',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class,'log_id');
    }

    public function friendRequest()
    {
        return $this->belongsTo(FriendRequest::class, 'log_id');
    }

    public function userActivity()
    {
        return $this->belongsTo(UserActivity::class, 'log_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'log_id');
    }
  
}
