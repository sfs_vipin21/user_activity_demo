<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Event;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::create([
            'user_id' => 1,
            'event_type' => 'Login',
        ]);

        Event::create([
            'user_id' => 2,
            'event_type' => 'Login',
        ]);

        Event::create([
            'user_id' => 3,
            'event_type' => 'Login',
        ]);

        Event::create([
            'user_id' => 2,
            'event_type' => 'Profile Updated',
        ]);
    }
}
