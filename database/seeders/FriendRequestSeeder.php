<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\FriendRequest;


class FriendRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FriendRequest::create([
            'user_id' => 1,
            'to_user_id' => 2,
            'request_status'=> 'Pending'
        ]);

        FriendRequest::create([
            'user_id' => 1,
            'to_user_id' => 3,
            'request_status'=> 'Accepted'
        ]);

        FriendRequest::create([
            'user_id' => 3,
            'to_user_id' => 1,
            'request_status'=> 'Pending'
        ]);

        FriendRequest::create([
            'user_id' => 4,
            'to_user_id' => 5,
            'request_status'=> 'Accepted'
        ]);

    }
}
