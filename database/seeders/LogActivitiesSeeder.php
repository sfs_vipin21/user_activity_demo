<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\LogActivity;

use App\Models\Post;
use App\Models\FriendRequest;
use App\Models\Event;
use App\Models\UserActivity;

use Log;

class LogActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::orderBy('created_at', 'desc')->limit(5)->get();

        foreach($posts as $post)
        {

            LogActivity::create([
                'user_id' => $post->user_id,
                'log_id' => $post->id,
                'log_type' => 'Post',
            ]);
        }

        $requests = FriendRequest::orderBy('created_at', 'desc')->limit(5)->get();
        foreach($requests as $request)
        {
            LogActivity::create([
                'user_id' => $request->user_id,
                'log_id' => $request->id,
                'log_type' => 'FriendRequest',
            ]);

        }

        $events = Event::orderBy('created_at', 'desc')->limit(5)->get();

        foreach($events as $event)
        {
            LogActivity::create([
                'user_id' => $event->user_id,
                'log_id' => $event->id,
                'log_type' => 'Event',
            ]);
        }
        
        $activities = UserActivity::orderBy('created_at', 'desc')->limit(5)->get();
        foreach($activities as $activity)
        {
            LogActivity::create([
                'user_id' => $activity->user_id,
                'log_id' => $activity->id,
                'log_type' => 'Activity',
            ]);
        }


    }
}
