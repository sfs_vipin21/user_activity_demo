<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\UserActivity;


class UserActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserActivity::create([
            'user_id' => 1,
            'activity_type' => 'Liked',
        ]);

        UserActivity::create([
            'user_id' => 1,
            'activity_type' => 'Posted',
        ]);

        UserActivity::create([
            'user_id' => 2,
            'activity_type' => 'Liked',
        ]);

        UserActivity::create([
            'user_id' => 10,
            'activity_type' => 'Posted',
        ]);

        
    }
}
