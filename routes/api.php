<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::get('users/sequential_check', [UserController::class, 'sequential_check']);
Route::get('users/concurrent_check', [UserController::class, 'concurrent_check']);


Route::get('users', [UserController::class, 'index']);
Route::get('users/{id}', [UserController::class, 'show']);
Route::get('users/logs/{id}', [UserController::class, 'userLogs']);

Route::get('posts', [PostController::class, 'index']);
Route::get('posts/{id}', [PostController::class, 'show']);

//Log Viewer
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::middleware('auth:api')->group(function(){
    Route::get('logout', [AuthController::class, 'logout']);

    //Api to View user events and activities
    // Route::get('users/logs/{id}', [UserController::class, 'userLogs']);

    //21/05/2021: Latest Modified API for User Logs 
    Route::post('users/activities', [UserController::class, 'logActivity']);
    
    // Route::get('users', [UserController::class, 'index']);
    // Route::get('users/{id}', [UserController::class, 'show']);


    // Route::get('posts', [PostController::class, 'index']);
    // Route::get('posts/{id}', [PostController::class, 'show']);

});
